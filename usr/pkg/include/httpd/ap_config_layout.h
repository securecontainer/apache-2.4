/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @file  ap_config_layout.h
 * @brief Apache Config Layout
 */

#ifndef AP_CONFIG_LAYOUT_H
#define AP_CONFIG_LAYOUT_H

/* Configured Apache directory layout */
#define DEFAULT_PREFIX "/usr/pkg"
#define DEFAULT_EXP_EXEC_PREFIX "/usr/pkg"
#define DEFAULT_REL_EXEC_PREFIX ""
#define DEFAULT_EXP_BINDIR "/usr/pkg/bin"
#define DEFAULT_REL_BINDIR "bin"
#define DEFAULT_EXP_SBINDIR "/usr/pkg/sbin"
#define DEFAULT_REL_SBINDIR "sbin"
#define DEFAULT_EXP_LIBEXECDIR "/usr/pkg/lib/httpd"
#define DEFAULT_REL_LIBEXECDIR "lib/httpd"
#define DEFAULT_EXP_MANDIR "/usr/pkg/man"
#define DEFAULT_REL_MANDIR "man"
#define DEFAULT_EXP_SYSCONFDIR "/usr/pkg/etc/httpd"
#define DEFAULT_REL_SYSCONFDIR "etc/httpd"
#define DEFAULT_EXP_DATADIR "/usr/pkg/share/httpd"
#define DEFAULT_REL_DATADIR "share/httpd"
#define DEFAULT_EXP_INSTALLBUILDDIR "/usr/pkg/share/httpd/build"
#define DEFAULT_REL_INSTALLBUILDDIR "share/httpd/build"
#define DEFAULT_EXP_ERRORDIR "/usr/pkg/share/httpd/error"
#define DEFAULT_REL_ERRORDIR "share/httpd/error"
#define DEFAULT_EXP_ICONSDIR "/usr/pkg/share/httpd/icons"
#define DEFAULT_REL_ICONSDIR "share/httpd/icons"
#define DEFAULT_EXP_HTDOCSDIR "/usr/pkg/share/httpd/htdocs"
#define DEFAULT_REL_HTDOCSDIR "share/httpd/htdocs"
#define DEFAULT_EXP_MANUALDIR "/usr/pkg/share/httpd/manual"
#define DEFAULT_REL_MANUALDIR "share/httpd/manual"
#define DEFAULT_EXP_CGIDIR "/usr/pkg/libexec/cgi-bin"
#define DEFAULT_REL_CGIDIR "libexec/cgi-bin"
#define DEFAULT_EXP_INCLUDEDIR "/usr/pkg/include/httpd"
#define DEFAULT_REL_INCLUDEDIR "include/httpd"
#define DEFAULT_EXP_LOCALSTATEDIR "/usr/pkg/var"
#define DEFAULT_REL_LOCALSTATEDIR "var"
#define DEFAULT_EXP_RUNTIMEDIR "/usr/pkg/var/run"
#define DEFAULT_REL_RUNTIMEDIR "var/run"
#define DEFAULT_EXP_LOGFILEDIR "/usr/pkg/var/log/httpd"
#define DEFAULT_REL_LOGFILEDIR "var/log/httpd"
#define DEFAULT_EXP_PROXYCACHEDIR "/usr/pkg/var/db/httpd/proxy"
#define DEFAULT_REL_PROXYCACHEDIR "var/db/httpd/proxy"

#endif /* AP_CONFIG_LAYOUT_H */
