# Overview:

The Apache HTTP Server Project is an effort to develop and maintain an open-source HTTP server for modern operating systems including UNIX and Windows. The goal of this project is to provide a secure, efficient and extensible server that provides HTTP services in sync with the current HTTP standards.

The Apache HTTP Server ("httpd") was launched in 1995 and it has been the most popular web server on the Internet since April 1996. It has celebrated its 20th birthday as a project in February 2015.

## Homepage

[Apache Webserver](https://httpd.apache.org)

## Notifications (updates):

## Changes done:

- disable man page download
- remove example download
- allow only minimum download from modules

## TODO:

